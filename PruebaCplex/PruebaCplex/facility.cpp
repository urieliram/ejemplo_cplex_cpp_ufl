// -----------------------// -------------------------------------------------------------- -*- C++ -*-
// File: facility.cpp
// Version 12.6  
// --------------------------------------------------------------------------
// Licensed Materials - Property of IBM
// 5725-A06 5725-A29 5724-Y48 5724-Y49 5724-Y54 5724-Y55 5655-Y21
// Copyright IBM Corporation 2000, 2013. All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
// --------------------------------------------------------------------------
//

#include <ilcplex/ilocplex.h>

ILOSTLBEGIN

typedef IloArray<IloNumArray>    FloatMatrix;
typedef IloArray<IloNumVarArray> NumVarMatrix;

int
main(int argc, char **argv)
{
   IloEnv env;
   try {
      
	  // Se declaran los indices
      IloInt i, j , k;

	  // Caso del ejemplo basico
      IloNumArray capacity(env), fixedCost(env);
      FloatMatrix cost(env);
      IloInt      nbLocations;
      IloInt      nbClients;
	  	  
      //const char* filename  = "facility.dat";
      const char* filename  = "facility_ULF.dat";
      if (argc > 1) 
         filename = argv[1];
      ifstream file(filename);
      if (!file) {
         cerr << "ERROR: could not open file '" << filename
              << "' for reading" << endl;
         cerr << "usage:   " << argv[0] << " <file>" << endl;
         throw(-1);
      }

      file >> capacity >> fixedCost >> cost;
      nbLocations = capacity.getSize();
      nbClients   = cost.getSize(); 

	  //Validación de las dimensiones de entrada
      IloBool consistentData = (fixedCost.getSize() == nbLocations);
      for(i = 0; consistentData && (i < nbClients); i++)
         consistentData = (cost[i].getSize() == nbLocations);
      if (!consistentData) {
         cerr << "ERROR: data file '" 
              << filename << "' contains inconsistent data" << endl;
         throw(-1);
      }

      IloNumVarArray open(env, nbLocations, 0, 1, ILOINT); //cotas
      NumVarMatrix supply(env, nbClients);
      for(i = 0; i < nbClients; i++)
         supply[i] = IloNumVarArray(env, nbLocations, 0, 1, ILOINT);
	
	//Se agrega el grupo de restricciones de satisfacción de la demanda.
      IloModel model(env);
      for(i = 0; i < nbClients; i++) //paratodo i clientes, genera restricciones.
         model.add(IloSum(supply[i]) == 1); //>= , ==, <= significa el sentido de la desigualdad en la optimización
         //model.add(IloSum(supply[i]) == 1);
	  
	////Se agrega otro grupo de restricciones de capacidad.	  
 //     for(j = 0; j < nbLocations; j++) { //paratodo j depósitos, genera restricciones.
 //        IloExpr constr(env); //constr es la restriccion
 //        for(i = 0; i < nbClients; i++)// sumatorias en indice i
 //           constr += supply[i][j];
 //        model.add(constr <= capacity[j] * open[j]); //constr es el lado izquierdo  y lado derecho de la restriccion 
 //        constr.end();//teminar grupo de restricciones
 //     }

	  	//Se agrega otro grupo de restricciones de condiciones de apertura o no del depósito.	  
      for(j = 0; j < nbLocations; j++) { //paratodo j depósitos, genera restricciones.
         for(i = 0; i < nbClients; i++){ // sumatorias en indice i
         IloExpr constr(env); //declaracion "al vuelo" de la restriccion constr
                   constr += -supply[i][j] ;
         model.add(constr >= - open[j]); //v es el lado izquierdo y lado derecho de la restriccion 
         constr.end(); //teminar grupo de restricciones
		 }
      }


	  //obj es la FUNCION OBJETIVO se mete una por una.
      IloExpr obj = IloScalProd(fixedCost, open);
      for(i = 0; i < nbClients; i++) {    
         obj += IloScalProd(cost[i], supply[i]);
      }
      //model.add(IloMinimize(env, obj)); //maximizar o minimizar
      model.add(IloMaximize(env, obj)); //maximizar o minimizar
      obj.end();

	  //se abre el ambiente de CPLEX
      IloCplex cplex(env);
      cplex.extract(model);
      cplex.solve();
	
      cplex.out() << "Solution status: " << cplex.getStatus() << endl;

	  cplex.exportModel("Model.lp");

      IloNum tolerance = cplex.getParam(IloCplex::Param::MIP::Tolerances::Integrality);



      cplex.out() << "valor optimo : " << cplex.getObjValue() << endl;
      for(j = 0; j < nbLocations; j++) {
         if (cplex.getValue(open[j]) >= 1 - tolerance) {
            cplex.out() << "Facility " << j << " is open, it serves clients ";
            for(i = 0; i < nbClients; i++) {
               if (cplex.getValue(supply[i][j]) >= 1 - tolerance)
                  cplex.out() << i << " ";
            }
            cplex.out() << endl; 
         }
      }


	  

   }
   catch(IloException& e) {
      cerr  << " ERROR: " << e << endl;   
   }
   catch(...) {
      cerr  << " ERROR" << endl;   
   }
   env.end();
   system("pause");
   return 0;
}

